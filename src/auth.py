from typing import Annotated

import httpx
from fastapi import Depends, HTTPException
from fastapi.security import OAuth2PasswordBearer
from jose import jwt
from starlette import status

from config import settings
from models import user_service, request

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token", auto_error=False)

credentials_exception = HTTPException(
    status_code=status.HTTP_401_UNAUTHORIZED,
    detail="Could not validate credentials",
    headers={"WWW-Authenticate": "Bearer"},
)


async def validate_token(token: Annotated[str, Depends(oauth2_scheme)]):
    if token is None:
        return None
    dirty_claims = jwt.get_unverified_claims(token)
    dirty_header = jwt.get_unverified_headers(token)
    provider = dirty_claims.get("iss")
    if provider not in settings.providers:
        raise credentials_exception
    async with httpx.AsyncClient() as client:
        keys_result = await client.get(f"{provider}.well-known/jwks.json")
        keys = keys_result.json().get("keys", [])
        key = [
            {
                "kty": key["kty"],
                "kid": key["kid"],
                "use": key["use"],
                "n": key["n"],
                "e": key["e"],
            }
            for key in keys
            if key.get("kid") == dirty_header.get("kid", "")
            and key.get("alg") == dirty_header.get("alg", "RS256")
        ][0]
    try:
        claims = jwt.decode(token, key, audience=dirty_claims.get("aud"))
    except (jwt.ExpiredSignatureError, jwt.JWTClaimsError) as e:
        raise credentials_exception from e
    return claims


async def create_user(claims: Annotated[dict, Depends(validate_token)]):
    if claims is None:
        raise credentials_exception
    async with httpx.AsyncClient() as client:
        try:
            profile_result = await client.post(
                f"{settings.user_service_url}/v1/user",
                json=request.ProfileCondensed(
                    first_name=claims.get("name"),
                    oauth_id=claims.get("sub"),
                    profile_picture=claims.get("picture"),
                ).dict(),
            )
            profile_result.raise_for_status()
            user = user_service.User.parse_raw(
                profile_result.content.decode(profile_result.default_encoding)  # type: ignore
            )
            return user
        except httpx.ConnectError as e:
            raise HTTPException(
                status_code=status.HTTP_503_SERVICE_UNAVAILABLE,
                detail="User-Service unavailable",
            ) from e


async def get_user(claims: Annotated[dict, Depends(validate_token)]):
    if claims is None:
        return None
    async with httpx.AsyncClient() as client:
        params = {"oauth_id": claims.get("sub")}
        try:
            profile_result = await client.get(
                f"{settings.user_service_url}/v1/user", params=params
            )
            profile_result.raise_for_status()
            user = user_service.User.parse_raw(
                profile_result.content.decode(profile_result.default_encoding)  # type: ignore
            )
            return user
        except httpx.ConnectError:
            return None
