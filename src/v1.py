import json
from typing import List, Optional, Annotated
from uuid import UUID

import httpx
from fastapi import APIRouter, HTTPException, Depends, Query
from starlette import status

import auth
from auth import create_user, get_user
from config import settings
from models import response
from models import listing_service
from models import user_service
from models.patch import JsonPatch
from models.response import ListingCondensedArray, PatchResponse
from models.sensor_service import get_sensors

router = APIRouter(
    prefix="/v1",
    tags=["v1"],
)


@router.post("/register", status_code=201)
async def register_user_profile(
    user: Annotated[user_service.User, Depends(create_user)]
):
    if user is None:
        raise auth.credentials_exception
    profile = response.ProfileComplete(
        uid=user.uid,
        first_name=user.first_name,
        last_name=user.last_name,
        username=user.username,
        bio=user.bio,
        profile_picture=user.profile_picture,
        friends=[],  # ToDo: Get from GraphService
        blocking=[],  # ToDo: Get from GraphService
        genres=[],  # ToDo: Get from GraphService
    )
    return profile


@router.get(
    "/profile",
    response_model=response.ProfileComplete,
    response_model_exclude_none=True,
)
async def get_user_profile(user: Annotated[user_service.User, Depends(get_user)]):
    if user is None:
        raise auth.credentials_exception
    profile = response.ProfileComplete(
        uid=user.uid,
        first_name=user.first_name,
        last_name=user.last_name,
        username=user.username,
        bio=user.bio,
        profile_picture=user.profile_picture,
        spotify_authorization_token="**redacted**"
        if user.spotify_authorization_token is not None
        else None,
        apple_music_user_token="**redacted**"
        if user.apple_music_user_token is not None
        else None,
        friends=[],  # ToDo: Get from GraphService
        blocking=[],  # ToDo: Get from GraphService
        genres=[],  # ToDo: Get from GraphService
    )
    return profile


@router.patch("/profile", response_model=PatchResponse)
async def patch_user_profile(
    body: List[JsonPatch], user: Annotated[user_service.User, Depends(get_user)]
):
    if user is None:
        raise auth.credentials_exception
    async with httpx.AsyncClient() as client:
        try:
            patch_result = await client.patch(
                f"{settings.user_service_url}/v1/user/{user.uid}",
                json=[item.dict(exclude_none=True) for item in body],
            )
            patch_result.raise_for_status()
        except httpx.HTTPStatusError as e:
            raise HTTPException(
                status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                detail="Error processing patch",
            ) from e
        except httpx.ConnectError as e:
            raise HTTPException(
                status_code=status.HTTP_503_SERVICE_UNAVAILABLE,
                detail="User-Service unavailable",
            ) from e
    return PatchResponse.parse_raw(
        patch_result.content.decode(patch_result.default_encoding)  # type: ignore
    )


@router.get(
    "/listing",
    response_model=ListingCondensedArray,
    response_model_exclude_none=True,
)
async def get_listings_condensed(
    user: Annotated[  # pylint: disable=unused-argument
        user_service.User, Depends(get_user)
    ],
    search: Annotated[str | None, Query()] = None,
    tags: Annotated[list[str] | None, Query()] = None,
    exclude_tags: Annotated[list[str] | None, Query()] = None,
    lat: Annotated[float | None, Query(ge=-90.0, le=90.0)] = None,
    lon: Annotated[float | None, Query(ge=-180.0, le=180.0)] = None,
    radius: Optional[float] = None,
    rating: Optional[str] = None,
    page: Annotated[int | None, Query(ge=1)] = 1,
):
    # ToDo: Sorting by graph service & distance
    base_query = {
        "tags": tags,
        "exclude_tags": exclude_tags,
        "lat": lat,
        "lon": lon,
        "radius": radius,
        "rating": rating,
        "page": page,
    }
    if page < 1:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="page cannot be less than 1",
        )
    async with httpx.AsyncClient() as client:
        try:
            result = await client.get(
                f"{settings.listing_service_url}/v1/listing",
                params={key: val for key, val in base_query.items() if val is not None},
            )
        except httpx.ConnectError as e:
            raise HTTPException(
                status_code=status.HTTP_503_SERVICE_UNAVAILABLE,
                detail="Listing-Service unavailable",
            ) from e
        if result.status_code == 422:
            raise HTTPException(
                status_code=422,
                detail="unprocessable request: if using position then `lat`, `lon`, and `radius` must be present",
            )
        result.raise_for_status()
        listings_condensed = []
        listing_array = listing_service.ListingCondensedArray.parse_raw(
            result.content.decode(result.default_encoding)  # type: ignore
        )
        for item in listing_array.data:
            listing = response.ListingCondensed.from_orm(item)
            listing.favorite = False  # todo: from graphdb
            listings_condensed.append(listing)
    _next = {
        "search": search,
        "tags": ",".join(tags) if tags else None,
        "lat": lat,
        "lon": lon,
        "radius": radius,
        "rating": rating,
        "page": page + 1,
    }
    _next_str = f"{router.prefix}/listing?"
    first = True
    for key, val in _next.items():
        if val is not None:
            if not first:
                _next_str += "&"
            _next_str += f"{key}={val}"
            first = False
    return ListingCondensedArray(
        data=listings_condensed,
        _count=listing_array.count,
        _next=_next_str if listing_array.next is not None else None,
    )


@router.get(
    "/listing/{uid}",
    response_model=response.ListingComplete,
    response_model_exclude_none=True,
)
async def get_listing_by_uid(
    user: Annotated[  # pylint: disable=unused-argument
        user_service.User, Depends(get_user)
    ],
    uid: UUID,
):
    # ToDo: Sorting by graph service & distance
    async with httpx.AsyncClient() as client:
        try:
            listing_result = await client.get(
                f"{settings.listing_service_url}/v1/listing/{uid}",
            )
        except (httpx.ConnectError, httpx.HTTPStatusError) as e:
            raise HTTPException(
                status_code=status.HTTP_503_SERVICE_UNAVAILABLE,
                detail="Listing-Service unavailable",
            ) from e
        listing = listing_service.ListingComplete.parse_raw(
            listing_result.content.decode(listing_result.default_encoding)  # type: ignore
        )
        sensors = [sensor async for sensor in get_sensors(client, listing.sensors)]
        sensor = sensors[0] if len(sensors) > 0 else None
        final_result = response.ListingComplete(
            **listing.dict(exclude={"sensors"}),
            sensors=sensors,
            primary_sensor=sensor.sensor_uid if sensor is not None else None,
            favorite=False,  # From Graph Service
        )
    return final_result


@router.get(
    "/listing/{uid}/opening_time",
    response_model=response.OpeningTimeArray,
    response_model_exclude_none=True,
)
async def get_openings_by_listing_uid(uid: UUID):
    async with httpx.AsyncClient() as client:
        try:
            listing_opening_result = await client.get(
                f"{settings.listing_service_url}/v1/listing/{uid}/opening_time",
            )
        except (httpx.ConnectError, httpx.HTTPStatusError) as e:
            raise HTTPException(
                status_code=status.HTTP_503_SERVICE_UNAVAILABLE,
                detail="Listing-Service unavailable",
            ) from e
        opening_times_raw = listing_service.OpeningTimeArray.parse_raw(
            listing_opening_result.content.decode(
                listing_opening_result.default_encoding  # type: ignore
            )
        )
        return response.OpeningTimeArray.from_orm(opening_times_raw)


@router.get(
    "/listing/{uid}/sensor",
    response_model=List[response.SensorComplete],
    response_model_exclude_none=True,
)
async def get_sensors_by_listing_uid(uid: UUID):
    async with httpx.AsyncClient() as client:
        try:
            listing_sensor_result = await client.get(
                f"{settings.listing_service_url}/v1/listing/{uid}/sensor",
            )
        except (httpx.ConnectError, httpx.HTTPStatusError) as e:
            raise HTTPException(
                status_code=status.HTTP_503_SERVICE_UNAVAILABLE,
                detail="Listing-Service unavailable",
            ) from e
        sensor_uids = json.loads(
            listing_sensor_result.content.decode(listing_sensor_result.default_encoding)  # type: ignore
        )
        try:
            result = await client.get(
                f"{settings.sensor_service_url}/health",
            )
            result.raise_for_status()
        except (httpx.ConnectError, httpx.HTTPStatusError) as e:
            raise HTTPException(
                status_code=status.HTTP_503_SERVICE_UNAVAILABLE,
                detail="Sensor-Service unavailable",
            ) from e
        return [sensor async for sensor in get_sensors(client, sensor_uids)]


@router.get(
    "/listing/{uid}/sensor/{sensor_uid}",
    response_model=response.SensorComplete,
    response_model_exclude_none=True,
)
async def get_sensor_by_uids(uid: UUID, sensor_uid: UUID):
    async with httpx.AsyncClient() as client:
        try:
            listing_sensor_result = await client.get(
                f"{settings.listing_service_url}/v1/listing/{uid}/sensor",
            )
            listing_sensor_result.raise_for_status()
        except httpx.ConnectError as e:
            raise HTTPException(
                status_code=status.HTTP_503_SERVICE_UNAVAILABLE,
                detail="Sensor-Service unavailable",
            ) from e
        sensor_uids = json.loads(
            listing_sensor_result.content.decode(listing_sensor_result.default_encoding)  # type: ignore
        )
        if str(sensor_uid) not in sensor_uids:
            raise HTTPException(status_code=403, detail="Forbidden access attempt")
        return await anext(get_sensors(client, [sensor_uid]))


@router.get(
    "/listing/{uid}/photo",
    response_model=response.PhotoArray,
    response_model_exclude_none=True,
)
async def get_photos_by_listing_uid(uid: UUID):
    async with httpx.AsyncClient() as client:
        try:
            listing_photo_result = await client.get(
                f"{settings.listing_service_url}/v1/listing/{uid}/photo",
            )
        except httpx.ConnectError as e:
            raise HTTPException(
                status_code=status.HTTP_503_SERVICE_UNAVAILABLE,
                detail="Listing-Service unavailable",
            ) from e
        listing_photo_result.raise_for_status()
        photo_raw = listing_service.PhotoArray.parse_raw(
            listing_photo_result.content.decode(listing_photo_result.default_encoding)  # type: ignore
        )
        return response.PhotoArray.from_orm(photo_raw)
