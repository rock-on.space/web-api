import logging

import httpx
from fastapi import FastAPI
from slowapi import Limiter, _rate_limit_exceeded_handler
from slowapi.errors import RateLimitExceeded
from slowapi.middleware import SlowAPIASGIMiddleware
from slowapi.util import get_remote_address
from starlette.middleware.cors import CORSMiddleware
from starlette.middleware.sessions import SessionMiddleware

import v1
from config import settings
from models.response import Health
from security_headers_middleware import SecurityHeadersMiddleware


app = FastAPI()

app.add_middleware(SecurityHeadersMiddleware)
app.add_middleware(
    CORSMiddleware,
    allow_origins=settings.origins,
    allow_credentials=True,
    allow_methods=["POST", "GET", "PUT", "OPTIONS"],
    allow_headers=["*"],
    max_age=3600,
)
app.add_middleware(SessionMiddleware, secret_key=settings.session_key)

limiter = Limiter(key_func=get_remote_address, default_limits=["10/second"])
app.state.limiter = limiter
app.add_exception_handler(RateLimitExceeded, _rate_limit_exceeded_handler)
app.add_middleware(SlowAPIASGIMiddleware)

app.include_router(v1.router)


@app.get("/healthz", response_model=Health)
async def healthz():
    response = Health(errors=[])
    async with httpx.AsyncClient() as client:
        try:
            user_service_result = await client.get(
                f"{settings.user_service_url}/healthz",
            )
            user_service_result.raise_for_status()
        except httpx.ConnectError as e:
            logging.warning("user_service: %s", e.args)
            response.errors.append("user_service")
        except httpx.HTTPStatusError as e:
            logging.warning("user_service: %s", e.args)
            response.errors.append("user_service")
        try:
            listing_service_result = await client.get(
                f"{settings.listing_service_url}/healthz",
            )
            listing_service_result.raise_for_status()
        except httpx.ConnectError as e:
            logging.warning("listing_service: %s", e.args)
            response.errors.append("listing_service")
        except httpx.HTTPStatusError as e:
            logging.warning("listing_service: %s", e.args)
            response.errors.append("listing_service")
        try:
            sensor_service_result = await client.get(
                f"{settings.sensor_service_url}/healthz",
            )
            sensor_service_result.raise_for_status()
        except httpx.ConnectError as e:
            logging.warning("sensor_service: %s", e.args)
            response.errors.append("sensor_service")
        except httpx.HTTPStatusError as e:
            logging.warning("sensor_service: %s", e.args)
            response.errors.append("sensor_service")
    return response


@app.get("/alive")
async def alive():
    return {"message": "Hello World"}
