import re
from enum import StrEnum, auto
from typing import Optional

from pydantic import BaseModel, Field, validator


class JsonPatchOperations(StrEnum):
    add = auto()  # type: ignore
    remove = auto()  # type: ignore
    replace = auto()  # type: ignore
    move = auto()  # type: ignore
    copy = auto()  # type: ignore
    test = auto()  # type: ignore


class JsonPatch(BaseModel):
    """
    RFC 6902 - JavaScript Object Notation (JSON) Patch

    This represents a single line of the patch, the standard calls for a JSON Array of these.
    """

    value: Optional[object]
    path: str
    from_path: Optional[str] = Field(None, alias="from")
    op: JsonPatchOperations

    @validator("path")
    def check_path(cls, path):
        if not re.match(r"(^/\S*$)", path):
            raise ValueError("invalid path")
        return path

    @validator("from_path")
    def check_from_path(
        cls, from_path, values, **kwargs
    ):  # pylint:disable=unused-argument
        if values is None:
            values = {}
        if not re.match(r"(^/\S*$)", from_path) and from_path != values["path"]:
            raise ValueError("invalid path")
        return from_path

    @validator("op")
    def check_op(cls, op, values, **kwargs):  # pylint:disable=unused-argument
        if values is None:
            values = {}
        if op in ("move", "copy") and "from_path" not in values:
            raise ValueError("missing from")
        return op
