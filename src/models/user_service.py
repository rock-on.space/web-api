from typing import Optional
from uuid import UUID

from pydantic import BaseModel


class User(BaseModel):
    uid: UUID
    oauth_id: str
    username: Optional[str]
    first_name: Optional[str]
    last_name: Optional[str]
    bio: Optional[str]
    preferences: Optional[object]
    profile_picture: Optional[str]
    spotify_authorization_token: Optional[str]
    apple_music_user_token: Optional[str]


class Error(BaseModel):
    code: str
    message: str
