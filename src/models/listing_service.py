from datetime import datetime
from enum import StrEnum, auto
from typing import List, Optional, Set
from uuid import UUID

from pydantic import BaseModel, Field, NoneStr


class Repeating(StrEnum):
    Daily = auto()
    Weekly = auto()
    No = auto()


class PriceCategory(StrEnum):
    Cheap = auto()
    Average = auto()
    Expensive = auto()


class OpeningTime(BaseModel):
    uid: Optional[UUID]
    open: datetime
    close: datetime
    repeating: Repeating

    class Config:
        orm_mode = True


class OpeningTimeArray(BaseModel):
    next: NoneStr = Field(None, alias="_next")
    count: int = Field(None, alias="_count")
    data: List[OpeningTime]

    class Config:
        orm_mode = True


class Geometry(BaseModel):
    type: str
    coordinates: List[float]


class Photo(BaseModel):
    uid: Optional[UUID]
    photo: str
    alt: str

    class Config:
        orm_mode = True


class PhotoArray(BaseModel):
    next: NoneStr = Field(None, alias="_next")
    count: int = Field(None, alias="_count")
    data: List[Photo]

    class Config:
        orm_mode = True


class ListingMinimal(BaseModel):
    uid: UUID
    name: str
    icon: str
    genres: Set[str]
    type: str
    tags: Set[str]
    open: bool

    class Config:
        orm_mode = True


class Geo(BaseModel):
    type: str
    geometry: Geometry
    properties: Optional[ListingMinimal]

    class Config:
        orm_mode = True


class ListingCondensed(ListingMinimal):
    geo: Geo

    class Config:
        orm_mode = True


class ListingCondensedArray(BaseModel):
    next: NoneStr = Field(None, alias="_next")
    count: int = Field(None, alias="_count")
    data: List[ListingCondensed]

    class Config:
        orm_mode = True


class ListingComplete(ListingCondensed):
    description: str
    address: str
    price_category: str
    rating: float
    photos: PhotoArray
    sensors: List[UUID]
    header_photo: Optional[str]
    opening_times: OpeningTimeArray

    class Config:
        orm_mode = True
