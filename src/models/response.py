from datetime import datetime, date
from enum import StrEnum, auto
from uuid import UUID

from pydantic import BaseModel, NoneStr, Field
from typing import List, Optional, Set

from config import settings


class Repeating(StrEnum):
    Daily = auto()
    Weekly = auto()
    No = auto()


class Health(BaseModel):
    service_name: str = settings.app_name
    version_number: str = settings.version_number
    errors: List[str]


class Geometry(BaseModel):
    type: str
    coordinates: List[float]

    class Config:
        orm_mode = True


class ProfileCondensed(BaseModel):
    uid: UUID
    first_name: Optional[str]
    last_name: Optional[str]
    username: Optional[str]
    bio: Optional[str]
    profile_picture: Optional[str]
    spotify_authorization_token: Optional[str]
    apple_music_user_token: Optional[str]

    class Config:
        orm_mode = True


class ProfileComplete(ProfileCondensed):
    blocking: List[str]
    friends: List[ProfileCondensed]


class ListingMinimal(BaseModel):
    uid: UUID
    name: str
    icon: str
    genres: Set[str]
    type: str
    favorite: Optional[bool]
    open: bool
    tags: Set[str]

    class Config:
        orm_mode = True


class Geo(BaseModel):
    type: str
    geometry: Geometry
    properties: ListingMinimal

    class Config:
        orm_mode = True


class ListingCondensed(ListingMinimal):
    geo: Geo

    class Config:
        orm_mode = True


class ListingCondensedArray(BaseModel):
    next: NoneStr = Field(None, alias="_next")
    count: int = Field(None, alias="_count")
    data: List[ListingCondensed]


class Genre(BaseModel):
    genre_uid: Optional[UUID]
    name: str


class Track(BaseModel):
    name: str
    album_image: str
    release_date: date
    artists: List[str]
    genres: List[Genre]
    timestamp: datetime

    class Config:
        orm_mode = True


class MusicArray(BaseModel):
    next: NoneStr = Field(None, alias="_next")
    count: int = Field(None, alias="_count")
    data: List[Track]

    class Config:
        orm_mode = True


class OpeningTime(BaseModel):
    open: datetime
    close: datetime
    repeating: Repeating

    class Config:
        orm_mode = True


class OpeningTimeArray(BaseModel):
    next: NoneStr = Field(None, alias="_next")
    count: int = Field(None, alias="_count")
    data: List[OpeningTime]

    class Config:
        orm_mode = True


class Photo(BaseModel):
    uid: Optional[UUID]
    photo: str
    alt: str

    class Config:
        orm_mode = True


class PhotoArray(BaseModel):
    next: NoneStr = Field(None, alias="_next")
    count: int = Field(None, alias="_count")
    data: List[Photo]

    class Config:
        orm_mode = True


class SensorComplete(BaseModel):
    sensor_uid: Optional[UUID]
    temperature: Optional[float]
    humidity: Optional[float]
    air_quality: Optional[str]
    current_track: Optional[Track]
    previous_tracks: Optional[MusicArray]

    class Config:
        orm_mode = True


class ListingComplete(ListingCondensed):
    description: str
    address: str
    price_category: str
    rating: float
    primary_sensor: Optional[UUID]
    sensors: List[SensorComplete]
    opening_times: OpeningTimeArray
    photos: PhotoArray
    header_photo: str

    class Config:
        orm_mode = True


class PatchResponse(BaseModel):
    count: int
    successes: int
    errors: List[str]


def voc2iaq(voc: float) -> str:
    if voc < 0.065:
        return "Excellent"
    if voc < 0.22:
        return "Good"
    if voc < 0.66:
        return "Moderate"
    if voc < 2.2:
        return "Poor"
    if voc < 5.5:
        return "Unhealthy"
    return "Unknown"
