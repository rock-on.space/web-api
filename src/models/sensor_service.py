from __future__ import annotations

from datetime import datetime, date
from enum import StrEnum, auto
from typing import Optional, List
from uuid import UUID

import httpx
from pydantic import BaseModel, NoneStr, Field

from config import settings
from models import response


class SensorEnviron(BaseModel):
    sensor_uid: UUID
    sound_level: Optional[float] = None
    music_level: Optional[float] = None
    temperature: float
    humidity: float
    voc: Optional[float] = None
    co2: Optional[float] = None

    class Config:
        orm_mode = True


class Genre(BaseModel):
    genre_uid: Optional[UUID]
    name: str

    class Config:
        orm_mode = True


class Track(BaseModel):
    name: str
    album_image: str
    release_date: date
    artists: List[str]
    genres: List[Genre]
    track_id: int
    detection_id: int
    sensor_uid: UUID
    timestamp: datetime

    class Config:
        orm_mode = True


class MusicArray(BaseModel):
    next: NoneStr = Field(None, alias="_next")
    count: int = Field(None, alias="_count")
    data: List[Track]

    class Config:
        orm_mode = True


class Repeating(StrEnum):
    Daily = auto()
    Weekly = auto()
    No = auto()


class SensorComplete(SensorEnviron):
    current_track: Track
    previous_tracks: MusicArray

    class Config:
        orm_mode = True


async def get_sensors(client: httpx.AsyncClient, sensors: List[UUID]):
    for sensor_uid in sensors:
        try:
            sensor_result = await client.get(
                f"{settings.sensor_service_url}/v1/sensor/{sensor_uid}"
            )
        except httpx.ConnectError:
            continue
        if sensor_result.status_code in [404, 204]:
            continue
        sensor_raw = SensorComplete.parse_raw(
            sensor_result.content.decode(sensor_result.default_encoding)  # type: ignore
        )
        sensor = response.SensorComplete.from_orm(sensor_raw)
        sensor.air_quality = response.voc2iaq(sensor_raw.voc)
        yield sensor
    return
