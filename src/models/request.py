from typing import Optional

from pydantic import BaseModel


class ProfileCondensed(BaseModel):
    first_name: Optional[str]
    last_name: Optional[str]
    oauth_id: str
    username: Optional[str]
    bio: Optional[str]
    profile_picture: Optional[str]
