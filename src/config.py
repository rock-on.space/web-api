from pydantic import BaseSettings


class Settings(BaseSettings):
    app_name: str = "APIGateway"
    version_number: str = "local"
    session_key: str = "REPLACE_ME"
    user_service_url: str = "REPLACE_ME"
    listing_service_url: str = "REPLACE_ME"
    sensor_service_url: str = "REPLACE_ME"
    origins: set[str] = {"https://app.rock-on.space", "https://www.rock-on.space"}
    providers: set[str] = {"https://rock-on-dev.eu.auth0.com/"}


settings = Settings()
